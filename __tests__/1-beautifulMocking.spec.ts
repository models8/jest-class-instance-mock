import { CacheService } from '../@fake_node_modules/CacheService';
import { FakeClass } from '../src/FakeClass';

jest.mock('../@fake_node_modules/CacheService');
const fakeClass = new FakeClass(new CacheService('', () => ''));

describe("✅ Will work and it's very short", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('test 1 ', async () => {
    const mockReadCache = jest.mocked(CacheService.prototype.readCache);
    mockReadCache.mockResolvedValue('TATA');
    const result = await fakeClass.makeSomething();
    console.log(result); //TATA
  });

  it('test 2', async () => {
    const result = await fakeClass.makeSomething();
    console.log(result); //undefined
  });
});
