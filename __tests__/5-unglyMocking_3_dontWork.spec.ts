import { CacheService } from '../@fake_node_modules/CacheService';
import { FakeClass } from '../src/FakeClass';

const mockReadCache = jest.fn(); /** ⁉️ Variable will not bring at the top of file by jest */
jest.mock('../@fake_node_modules/CacheService', () => ({
  /** Use syntactic sugar "mockReturnValue()" with external variable */
  CacheService: jest.fn().mockReturnValue({
    readCache: mockReadCache,
  }),
}));

const fakeClass = new FakeClass(new CacheService('', () => ''));

describe("❌ Use syntactic sugar 'mockReturnValue()' with external variable doesn't work ", () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('test 1 ', async () => {
    mockReadCache.mockResolvedValue('TATA');
    const result = await fakeClass.makeSomething();
    console.log(result); // Error triggered before this line
  });

  it('test 2 ', async () => {
    const result = await fakeClass.makeSomething();
    console.log(result);
  });
});
