import { CacheService } from '../@fake_node_modules/CacheService';
import { FakeClass } from '../src/FakeClass';

jest.mock('../@fake_node_modules/CacheService', () => ({
  /** Use syntactic sugar "mockReturnValue()" without external variable */
  CacheService: jest.fn().mockReturnValue({
    readCache: jest.fn().mockResolvedValue('TATA'),
  }),
}));

const fakeClass = new FakeClass(new CacheService('', () => ''));

describe("✅ Use syntactic sugar 'mockReturnValue()' without external variable work ", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });
  it('test 1 ', async () => {
    const result = await fakeClass.makeSomething();
    console.log(result); //TATA
  });

  it('test 2 ', async () => {
    const result = await fakeClass.makeSomething();
    console.log(result); //undefined
  });
});
