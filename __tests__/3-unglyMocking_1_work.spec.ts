import { CacheService } from '../@fake_node_modules/CacheService';
import { FakeClass } from '../src/FakeClass';

const mockReadCache = jest.fn();
jest.mock('../@fake_node_modules/CacheService', () => ({
  /** Place mock implementation into "jest.fn( --> implementation <-- )"  */
  CacheService: jest.fn(() => ({
    readCache: mockReadCache,
  })),
}));

const fakeClass = new FakeClass(new CacheService('', () => ''));

describe('✅ Will work but very verbose 🤯', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });
  it('test 1 ', async () => {
    mockReadCache.mockResolvedValue('TATA');
    const result = await fakeClass.makeSomething();
    console.log(result); //TATA
  });

  it('test 2 ', async () => {
    const result = await fakeClass.makeSomething();
    console.log(result); //undefined
  });
});
