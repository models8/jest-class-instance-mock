import { CacheService } from '../@fake_node_modules/CacheService';
import { FakeClass } from '../src/FakeClass';

const fakeClass = new FakeClass(new CacheService('', () => '')); //❗️ Throw an error when create CacheService instance caused by constructor bad params

describe('❌ will not work with spy', () => {
  beforeEach(() => {
    jest.restoreAllMocks();
  });

  it('test 1 ', async () => {
    const spyReadCache = jest.spyOn(CacheService.prototype, 'readCache');
    spyReadCache.mockResolvedValue('TATA');
    const result = await fakeClass.makeSomething();
    console.log(result); //--> Error before this step
  });
});
