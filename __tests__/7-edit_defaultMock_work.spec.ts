import { CacheService } from '../@fake_node_modules/CacheService';
import { FakeClass } from '../src/FakeClass';

jest.mock('../@fake_node_modules/CacheService', () => ({
  CacheService: jest.fn().mockReturnValue({
    readCache: () => 'TATA' /** Builds a real implementation ( without jest.fn() ) that cannot be reseted by resetAllMock() */,
  }),
}));

const fakeClass = new FakeClass(new CacheService('', () => ''));

describe('✅ The mock implementation will never be rested by resetAllMock()', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });
  it('test 1 ', async () => {
    const result = await fakeClass.makeSomething();
    console.log(result); //TATA
  });

  it('test 2 ', async () => {
    const result = await fakeClass.makeSomething();
    console.log(result); //TATA
  });
});
