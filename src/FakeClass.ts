import { CacheService } from '../@fake_node_modules/CacheService';

export class FakeClass {
  constructor(private cacheService: CacheService) {}

  public async makeSomething(): Promise<string> {
    return await this.cacheService.readCache('seed');
  }
}
